import React from "react";
import "./style.css";
import Filters from "./components/Filters";
import Launches from "./components/Launches";

const App = () => (
  <div className="wrapper">
    <p className="lg-text bold-text">SpaceX Launch Programs</p>
    <div className="responsive">
      <Filters />
      <Launches />
    </div>
    <footer>
      <span className="bold-text">Developed by:</span> Vaibhav Vinayak
    </footer>
  </div>
);

export default App;
