import React from "react";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import App from "./App";
import { render } from "@testing-library/react";

const mockStore = configureMockStore([thunk]);

const mockData = {
  isLoading: false,
  isError: false,
  error: null,
  launches: null,
  filterYear: null,
  filterLaunch: null,
  filterLand: null,
  filter: undefined,
};

describe("App component", () => {
  it("Render App", () => {
    const store = mockStore(mockData);
    render(
      <Provider store={store}>
        <App />
      </Provider>
    );
  });
});
