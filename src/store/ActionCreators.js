import Axios from "axios";
import { BASE_API } from "../constants/end-points";
import {
  fetchLaunchesRequest,
  fetchLaunchesSuccess,
  fetchLaunchesFailed,
} from "./Actions";

/**
 * @description Makes a GET call for fetching the Launches data
 * @param
 */
export const requestLaunches = (filter) => (dispatch) => {
  dispatch(fetchLaunchesRequest());
  return Axios.get(`${BASE_API}${filter ? filter : ""}`)
    .then((res) => dispatch(fetchLaunchesSuccess(res.data)))
    .catch((err) => dispatch(fetchLaunchesFailed(err?.message)));
};
