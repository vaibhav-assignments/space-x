import {
  FETCH_LAUNCHES_REQUEST,
  FETCH_LAUNCHES_SUCCESS,
  FETCH_LAUNCHES_FAILED,
  SET_FILTER_YEAR,
  SET_FILTER_LAUNCH,
  SET_FILTER_LAND,
  SET_FILTER,
} from "./ActionTypes";

/**
 * @description dispatches an action of FETCH_LAUNCHES_REQUEST type
 */
export const fetchLaunchesRequest = () => ({
  type: FETCH_LAUNCHES_REQUEST,
});

/**
 * @description dispatches an action of FETCH_LAUNCHES_SUCCESS type
 * @param {Array} - the launches array from the api response
 */
export const fetchLaunchesSuccess = (payload) => ({
  type: FETCH_LAUNCHES_SUCCESS,
  payload,
});

/**
 * @description dispatches an action of FETCH_LAUNCHES_FAILED type
 * @param {Array} - the launches array from the api response
 */
export const fetchLaunchesFailed = (payload) => ({
  type: FETCH_LAUNCHES_FAILED,
  payload,
});

/**
 * @description dispatches an action of SET_FILTER_YEAR type
 * @param {Number} - the launch year to filter the results
 */
export const setFilterYear = (payload) => ({
  type: SET_FILTER_YEAR,
  payload,
});

/**
 * @description dispatches an action of SET_FILTER_LAUNCH type
 * @param {Boolean} - the launch success flag to filter the results
 */
export const setFilterLaunch = (payload) => ({
  type: SET_FILTER_LAUNCH,
  payload,
});

/**
 * @description dispatches an action of SET_FILTER_LAND type
 * @param {Boolean} - the land success flag to filter the results
 */
export const setFilterLand = (payload) => ({
  type: SET_FILTER_LAND,
  payload,
});

/**
 * @description dispatches an action of SET_FILTER_LAND type
 * @param {String} - the actual filter to filter the results
 */
export const setFilter = (payload) => ({
  type: SET_FILTER,
  payload,
});
