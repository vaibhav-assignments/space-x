const {
  FETCH_LAUNCHES_REQUEST,
  FETCH_LAUNCHES_SUCCESS,
  FETCH_LAUNCHES_FAILED,
  SET_FILTER_YEAR,
  SET_FILTER_LAUNCH,
  SET_FILTER_LAND,
  SET_FILTER,
} = require("./ActionTypes");

const initialState = {
  isLoading: false,
  isError: false,
  error: null,
  launches: null,
  filterYear: null,
  filterLaunch: null,
  filterLand: null,
  filter: undefined,
};

const Reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case FETCH_LAUNCHES_REQUEST:
      return { ...state, isLoading: true, isError: false };
    case FETCH_LAUNCHES_SUCCESS:
      return { ...state, isLoading: false, launches: payload };
    case FETCH_LAUNCHES_FAILED:
      return { ...state, isLoading: false, isError: true, error: payload };
    case SET_FILTER_YEAR:
      return { ...state, filterYear: payload };
    case SET_FILTER_LAUNCH:
      return { ...state, filterLaunch: payload };
    case SET_FILTER_LAND:
      return { ...state, filterLand: payload };
    case SET_FILTER:
      return { ...state, filter: payload };
    default:
      return { ...state };
  }
};

export default Reducer;
