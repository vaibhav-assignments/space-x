import {
  FETCH_LAUNCHES_REQUEST,
  FETCH_LAUNCHES_SUCCESS,
  FETCH_LAUNCHES_FAILED,
  SET_FILTER_YEAR,
  SET_FILTER_LAUNCH,
  SET_FILTER_LAND,
  SET_FILTER,
} from "../ActionTypes";
import {
  fetchLaunchesRequest,
  fetchLaunchesSuccess,
  fetchLaunchesFailed,
  setFilterYear,
  setFilterLaunch,
  setFilterLand,
  setFilter,
} from "../Actions";

describe("Actions", () => {
  it("should create an action to dispatch FETCH_LAUNCHES_REQUEST", () => {
    const expectedAction = {
      type: FETCH_LAUNCHES_REQUEST,
    };
    expect(fetchLaunchesRequest()).toEqual(expectedAction);
  });
  it("should create an action to dispatch FETCH_LAUNCHES_SUCCESS", () => {
    const expectedAction = {
      type: FETCH_LAUNCHES_SUCCESS,
      payload: {},
    };
    expect(fetchLaunchesSuccess({})).toEqual(expectedAction);
  });
  it("should create an action to dispatch FETCH_LAUNCHES_FAILED", () => {
    const expectedAction = {
      type: FETCH_LAUNCHES_FAILED,
      payload: {},
    };
    expect(fetchLaunchesFailed({})).toEqual(expectedAction);
  });
  it("should create an action to dispatch SET_FILTER_YEAR", () => {
    const expectedAction = {
      type: SET_FILTER_YEAR,
      payload: 2012,
    };
    expect(setFilterYear(2012)).toEqual(expectedAction);
  });
  it("should create an action to dispatch SET_FILTER_LAUNCH", () => {
    const expectedAction = {
      type: SET_FILTER_LAUNCH,
      payload: false,
    };
    expect(setFilterLaunch(false)).toEqual(expectedAction);
  });
  it("should create an action to dispatch SET_FILTER_LAND", () => {
    const expectedAction = {
      type: SET_FILTER_LAND,
      payload: true,
    };
    expect(setFilterLand(true)).toEqual(expectedAction);
  });
  it("should create an action to dispatch SET_FILTER", () => {
    const expectedAction = {
      type: SET_FILTER,
      payload: "myFilter",
    };
    expect(setFilter("myFilter")).toEqual(expectedAction);
  });
});
