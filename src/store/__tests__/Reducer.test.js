import {
  FETCH_LAUNCHES_REQUEST,
  FETCH_LAUNCHES_SUCCESS,
  FETCH_LAUNCHES_FAILED,
  SET_FILTER_YEAR,
  SET_FILTER_LAUNCH,
  SET_FILTER_LAND,
  SET_FILTER,
} from "../ActionTypes";
import Reducer from "../Reducer";

const initialState = {
  isLoading: false,
  isError: false,
  error: null,
  launches: null,
  filterYear: null,
  filterLaunch: null,
  filterLand: null,
  filter: undefined,
};

describe("Reducer", () => {
  it("should return the initial state", () => {
    expect(Reducer(undefined, {})).toEqual(initialState);
  });
  it("should handle FETCH_LAUNCHES_REQUEST", () => {
    expect(Reducer([], { type: FETCH_LAUNCHES_REQUEST }));
  });
  it("should handle FETCH_LAUNCHES_SUCCESS", () => {
    expect(Reducer([], { type: FETCH_LAUNCHES_SUCCESS, payload: [1, 2] }));
  });
  it("should handle FETCH_LAUNCHES_FAILED", () => {
    expect(Reducer([], { type: FETCH_LAUNCHES_FAILED }));
  });
  it("should handle SET_FILTER_YEAR", () => {
    expect(Reducer([], { type: SET_FILTER_YEAR }));
  });
  it("should handle SET_FILTER_LAUNCH", () => {
    expect(Reducer([], { type: SET_FILTER_LAUNCH }));
  });
  it("should handle SET_FILTER_LAND", () => {
    expect(Reducer([], { type: SET_FILTER_LAND }));
  });
  it("should handle SET_FILTER", () => {
    expect(Reducer([], { type: SET_FILTER }));
  });
});
