import mockAxios from "axios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import {
  FETCH_LAUNCHES_REQUEST,
  FETCH_LAUNCHES_SUCCESS,
  FETCH_LAUNCHES_FAILED,
} from "../ActionTypes";
import { requestLaunches } from "../ActionCreators";

jest.mock("axios");
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("ActionCreators", () => {
  it("API return data with undefined filter", async () => {
    const mockData = [
      { flight_number: 6, mission_name: "Falcon 9 Test Flight" },
    ];
    const store = mockStore(mockData);
    const expectedActions = [
      { type: FETCH_LAUNCHES_REQUEST },
      { type: FETCH_LAUNCHES_SUCCESS, payload: mockData },
    ];
    mockAxios.get.mockResolvedValue({ data: mockData });
    await store.dispatch(requestLaunches());
    expect(store.getActions()).toEqual(expectedActions);
  });
  it("API return data with some filter", async () => {
    const mockData = [
      { flight_number: 6, mission_name: "Falcon 9 Test Flight" },
    ];
    const store = mockStore(mockData);
    const expectedActions = [
      { type: FETCH_LAUNCHES_REQUEST },
      { type: FETCH_LAUNCHES_SUCCESS, payload: mockData },
    ];
    mockAxios.get.mockResolvedValue({ data: mockData });
    await store.dispatch(requestLaunches("myFilter"));
    expect(store.getActions()).toEqual(expectedActions);
  });
  it("API return error", async () => {
    const message = "Network error";
    const store = mockStore({});
    const expectedActions = [
      { type: FETCH_LAUNCHES_REQUEST },
      { type: FETCH_LAUNCHES_FAILED, payload: message },
    ];
    mockAxios.get.mockRejectedValue({ message });
    await store.dispatch(requestLaunches());
    expect(store.getActions()).toEqual(expectedActions);
  });
});
