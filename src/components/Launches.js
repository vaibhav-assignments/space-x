import React, { useEffect } from "react";
import LaunchItem from "./LaunchItem";
import { connect } from "react-redux";
import { requestLaunches } from "../store/ActionCreators";
import ErrorMessage from "./ErrorMessage";
import { setFilter } from "../store/Actions";

const Launches = ({
  isLoading,
  isError,
  error,
  launches,
  filterYear,
  filterLaunch,
  filterLand,
  filter,
  fetchLaunches,
  applyFilter,
}) => {
  const resolveFilters = (a, b, c) => {
    if (a || b || c) {
      return [a, b, c].reduce((x, y) => {
        return `${x}${y ? `&${y}` : ""}`;
      });
    }
  };
  useEffect(() => {
    const yearFilter = filterYear ? `launch_year=${filterYear}` : undefined;
    const launchFilter =
      typeof filterLaunch === "boolean"
        ? `launch_success=${filterLaunch}`
        : undefined;
    const landFilter =
      typeof filterLand === "boolean"
        ? `land_success=${filterLand}`
        : undefined;
    if (yearFilter || launchFilter || landFilter) {
      applyFilter(`?${resolveFilters(yearFilter, launchFilter, landFilter)}`);
    }
  }, [filterYear, filterLaunch, filterLand, applyFilter]);
  useEffect(() => {
    fetchLaunches(filter);
  }, [filter, fetchLaunches]);
  return isLoading ? (
    <div className="loader" />
  ) : isError ? (
    <ErrorMessage message={error} />
  ) : launches?.length === 0 ? (
    <ErrorMessage message="No results found!" />
  ) : (
    <div className="launches-container">
      {launches?.map((launch, id) => (
        <LaunchItem key={id} launch={launch} />
      ))}
    </div>
  );
};

const mapStateToProps = (state) => ({ ...state });

const mapDispatchToProps = (dispatch) => ({
  fetchLaunches: (filter) => dispatch(requestLaunches(filter)),
  applyFilter: (filter) => dispatch(setFilter(filter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Launches);
