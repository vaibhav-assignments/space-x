import React from "react";
import { FILTER_END_YEAR, FILTER_START_YEAR } from "../constants";
import { connect } from "react-redux";
import {
  setFilterYear,
  setFilterLaunch,
  setFilterLand,
} from "../store/Actions";

const filterYears = new Array(Math.abs(FILTER_END_YEAR - FILTER_START_YEAR) + 1)
  .fill()
  .map(
    (el, id) =>
      id +
      (FILTER_START_YEAR < FILTER_END_YEAR
        ? FILTER_START_YEAR
        : FILTER_END_YEAR)
  );

const Filters = ({
  filterYear,
  filterLaunch,
  filterLand,
  applyFilterYear,
  applyFilterLaunch,
  applyFilterLand,
}) => {
  const isActive = (filter, value) =>
    filter === value ? " filter-item-active" : "";

  return (
    <div className="bg-white wrapper filters-wrapper">
      <div className="md-text bold-text" style={{ marginBottom: 24 }}>
        Filters
      </div>
      <div className="filter-category">Launch Year</div>
      <div className="filter-container">
        {filterYears.map((el, id) => (
          <button
            className={`filter-item${isActive(filterYear, el)}`}
            onClick={() => applyFilterYear(el)}
            key={id}
          >
            {el}
          </button>
        ))}
      </div>
      <div className="filter-category">Successful Launch</div>
      <div className="filter-container">
        <button
          className={`filter-item${isActive(filterLaunch, true)}`}
          onClick={() => applyFilterLaunch(true)}
          data-testid="successful-launch"
        >
          True
        </button>
        <button
          className={`filter-item${isActive(filterLaunch, false)}`}
          onClick={() => applyFilterLaunch(false)}
        >
          False
        </button>
      </div>
      <div className="filter-category">Successful Landing</div>
      <div className="filter-container">
        <button
          className={`filter-item${isActive(filterLand, true)}`}
          onClick={() => applyFilterLand(true)}
        >
          True
        </button>
        <button
          className={`filter-item${isActive(filterLand, false)}`}
          onClick={() => applyFilterLand(false)}
        >
          False
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({ ...state });

const mapDispatchToProps = (dispatch) => ({
  applyFilterYear: (year) => dispatch(setFilterYear(year)),
  applyFilterLaunch: (launch) => dispatch(setFilterLaunch(launch)),
  applyFilterLand: (land) => dispatch(setFilterLand(land)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
