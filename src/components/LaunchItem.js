import React, { Fragment } from "react";

const LaunchItem = ({ launch }) => (
  <div className="launch-item">
    <div className="launch-logo">
      <img
        height="150"
        src={launch?.links?.mission_patch_small}
        alt={`launch-${launch.mission_name}`}
      />
    </div>
    <Fragment>
      <p className="mission-title">
        {launch?.mission_name} #{launch?.flight_number}
      </p>
      {launch?.mission_id?.length > 0 && (
        <Fragment>
          <span className="bold-text">Mission Ids:</span>
          <ul className="mission-list">
            {launch?.mission_id?.map((missionid, id) => (
              <li key={id}>{missionid}</li>
            ))}
          </ul>
        </Fragment>
      )}
      <div>
        <span className="bold-text">Launch Year: </span>
        {launch.launch_year}
      </div>
      <div>
        <span className="bold-text">Successful Launch: </span>
        {typeof launch.launch_success === "boolean" &&
          `${launch.launch_success}`}
      </div>
      <div>
        <span className="bold-text">Successful Landing: </span>
        {typeof launch.launch_landing === "boolean" &&
          `${launch.launch_landing}`}
      </div>
    </Fragment>
  </div>
);

export default LaunchItem;
