import React from "react";

const ErrorMessage = ({ message }) =>
  message && (
    <div className="error">
      <p>{message}</p>
    </div>
  );

export default ErrorMessage;
