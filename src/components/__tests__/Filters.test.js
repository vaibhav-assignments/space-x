import React from "react";
import { render } from "@testing-library/react";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import Filters from "../Filters";

const mockStore = configureMockStore([thunk]);

const mockData = {
  isLoading: false,
  isError: false,
  error: null,
  launches: null,
  filterYear: null,
  filterLaunch: null,
  filterLand: null,
  filter: undefined,
};

describe("Filters component", () => {
  it("Render with Filter component", () => {
    const store = mockStore(mockData);
    render(
      <Provider store={store}>
        <Filters />
      </Provider>
    );
  });
  it("Clicking on a filter", () => {
    const store = mockStore(mockData);
    const { getByTestId } = render(
      <Provider store={store}>
        <Filters />
      </Provider>
    );
    getByTestId("successful-launch").click();
  });
});
