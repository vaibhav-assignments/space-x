import React from "react";
import { render } from "@testing-library/react";
import LaunchItem from "../LaunchItem";

describe("Filters component", () => {
  it("Render with Filter component", () => {
    render(
      <LaunchItem
        launch={[
          {
            flight_number: 6,
            mission_name: "Falcon 9 Test Flight",
            mission_id: ["EE86F74"],
          },
        ]}
      />
    );
  });
});
