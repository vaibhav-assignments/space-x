import React from "react";
import { render } from "@testing-library/react";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import Launches from "../Launches";

const mockStore = configureMockStore([thunk]);

const mockData = {
  isLoading: false,
  isError: false,
  error: null,
  launches: null,
  filterYear: null,
  filterLaunch: null,
  filterLand: null,
  filter: undefined,
};

describe("Launches component", () => {
  it("Render with isLoading", () => {
    mockData.isLoading = true;
    const store = mockStore(mockData);
    render(
      <Provider store={store}>
        <Launches />
      </Provider>
    );
  });
  it("Render with isError", () => {
    mockData.isLoading = false;
    mockData.isError = true;
    mockData.error = "Network Error";
    const store = mockStore(mockData);
    render(
      <Provider store={store}>
        <Launches />
      </Provider>
    );
  });
  it("Render normally", () => {
    mockData.isError = false;
    mockData.launches = [
      { a: 1, b: 2 },
      { c: 3, d: 4 },
    ];
    const store = mockStore(mockData);
    render(
      <Provider store={store}>
        <Launches />
      </Provider>
    );
  });
});
