# [SpaceX Labs](https://spacexlabs.netlify.app/) ![picture spacex-logo](https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/SpaceX-Logo-Xonly.svg/64px-SpaceX-Logo-Xonly.svg.png)

>This web app would help users list and browse all the launches by the [SpaceX](https://www.spacex.com/) program

## Table of Contents

- [Preview](#preview)
- [Tech Stack](#tech-stack)
- [Installation](#installation)
    - [Clone](#clone)
    - [Setup](#setup)
- [Build Configurations](#build-configurations)
    - [Local build](#local-build)
    - [Production build](#production-build)
    - [Deployment](#deployment)
- [Testing](#testing)
- [Unit Tests & Performance Metrics](#unit-tests-performance-metrics)

## Preview

### Mobile View

![mobile view 1](https://spacexlabs.netlify.app/images/mobile-view-1.png)
![mobile view 2](https://spacexlabs.netlify.app/images/mobile-view-2.png)

### Tablet View

![tablet view](https://spacexlabs.netlify.app/images/tablet-view.png)

### Desktop View

![desktop view](https://spacexlabs.netlify.app/images/desktop-view.png)

## Tech Stack

- **Frontend** - ReactJS
- **State Management** - Redux
- **Unit Testing** - React Testing Library

## Installation

Make sure you have the following software installed:
- `git`
- `nodejs`
- `npm` or `yarn`

### Clone

- Clone this repo to your local machine using `https://gitlab.com/VaibhavVinayak/space-x.git`

### Setup

- Using `npm`

```shell
$ git clone https://gitlab.com/VaibhavVinayak/space-x.git
$ cd space-x
$ npm install
```

- Using `yarn`

```shell
$ git clone https://gitlab.com/VaibhavVinayak/space-x.git
$ cd space-x
$ yarn
```

## Build Configurations

### Local build

>Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

- Using `npm`

```shell
npm start
```

- Using `yarn`

```shell
yarn start
```

### Production build

>Creates a production ready build. You can use a production server to serve this build.

- Using `npm`

```shell
npm run build
```

- Using `yarn`

```shell
yarn build
```

### Deployment

>Creates a production ready build and deploys it to a server.<br />Here we are using [Netlify](https://www.netlify.com/) to host our webapp.

- Using `npm`

```shell
npm run deploy
```

- Using `yarn`

```shell
yarn deploy
```

## Testing

>Runs the unit test cases written for the application.

- Using `npm`

```shell
npm test -- --watchAll
```

- Using `yarn`

```shell
yarn test ----watchAll
```

## Unit Tests & Performance Metrics

### Unit Tests

![lighthouse score](https://spacexlabs.netlify.app/images/unit-tests.png)

### Code coverage

>Actual coverage is higher than the screenshot due to a know issue in jest with newer version of react-scripts when run with `--coverage` flag

![lighthouse score](https://spacexlabs.netlify.app/images/code-coverage.png)


### Lighthouse Score

![lighthouse score](https://spacexlabs.netlify.app/images/lighthouse-score.png)